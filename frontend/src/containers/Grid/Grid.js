import React, { Component }  from 'react';
import './Grid.css';
import Historical from '../../components/financials/Historical/Historical';
import DonutNav from '../../components/graphs/DonutNav/DonutNav';
import InstrumentWrapper from '../InstrumentWrapper/InstrumentWrapper'

class grid extends Component {
    state = {
      instrumentNameList: this.props.instrumentNameList,
      instrumentData: this.props.instrumentData,
      graphSizeData: this.props.graphSizeData,
      currentInstrument: {
        "instrumentName": "No Instrument",
        "average": {
          "avgBuy": 0,
          "avgSell": 0,
          "currentBuy": 0,
          "currentSell": 0
        }
      }
    }

    getCurrentInstrument = (index) => {
      console.log("Grid.js:" + index)
      this.setState({
        currentInstrument: this.state.instrumentData[index]
      })
    }

    render() {
      console.log(this.state.instrumentNameList)
      return (
        <div className='grid-container'>
          <DonutNav instrumentNameList={this.state.instrumentNameList} 
          currentInstrument={this.state.currentInstrument} 
          graphSizeData={this.state.graphSizeData}
          click={this.getCurrentInstrument}/>
          <InstrumentWrapper currentInstrument={this.state.currentInstrument} className='instrumentWrapper'/>
          <Historical pageName='Historical' className='historical' />
        </div>
      );
    }

  }

  export default grid;