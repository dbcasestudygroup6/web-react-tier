import React from 'react';
import './InstrumentWrapper.css';
import Average from '../../components/graphs/Average/Average';
import Position from '../../components/financials/Position/Position';
import Effective from '../../components/financials/Effective/Effective';
import Realized from '../../components/financials/Realized/Realized';


const grid = (props) => {
    return (
      <div className='grid-container-instruments'>
        <Average pageName='Average' className='average' currentInstrument={props.currentInstrument} title={props.instrumentName}/>      
        <Position pageName='Position' className='position' value={props.currentInstrument.position}/>
        <Effective pageName='Effective' className='effective' value={props.currentInstrument.effective}/>
        <Realized pageName='Realized' className='realized' value={props.currentInstrument.realized}/>
      </div>
    );
  }
  
  export default grid;