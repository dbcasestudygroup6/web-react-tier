import React, { Component } from 'react';
import './Dashboard.css';
import DashBoard from '../../components/dashboard/DashBoard';
import Average from '../../components/graphs/Average/Average';
import Position from '../../components/financials/Position/Position';
import Effective from '../../components/financials/Effective/Effective';
import Realized from '../../components/financials/Realized/Realized';
import Historical from '../../components/financials/Historical/Historical';

class Dashboard extends Component {
  state = {
    //Could make a seperate data file of pages
    pages: [
          {name: 'DashBoard'},
          {name: 'Average'},
          {name: 'Position'},
          {name: 'Effective'},
          {name: 'Realized'},
          {name: 'Historical'}
    ],
    currentPage: 'DashBoard'
  }

  getCurrentPage = (newCurrent) => {
    this.setState({
      currentPage: newCurrent
    })
  }

  render() {

    let page = null;

    switch(this.state.currentPage) {
      case this.state.pages[0].name:
        page = (<DashBoard pageName={this.state.currentPage}/>);
        break;
      case this.state.pages[1].name:
        page = (<Average pageName={this.state.currentPage}/>);
        break;
      case this.state.pages[2].name:
        page = (<Position pageName={this.state.currentPage}/>);
        break;
      case this.state.pages[3].name:
        page = (<Effective pageName={this.state.currentPage}/>);
        break;
      case this.state.pages[4].name:
          page = (<Realized pageName={this.state.currentPage}/>);
          break;
      case this.state.pages[5].name:
          page = (<Historical pageName={this.state.currentPage}/>);
          break;
      default:
        page = (<DashBoard />);
    }
    return (
      <div className="App-Dashboard">

      {page}
      </div>
    );
  }
}

export default Dashboard;