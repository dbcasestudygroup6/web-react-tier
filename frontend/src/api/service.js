const SERVER = 'http://192.168.99.100:5000';
const LOGIN = '/login';
const GET_DATA = '/get_data';

function fetch_status_check(response) {
    if (!response.ok) {
        if ([401, 403].indexOf(response.status) !== -1) {
            logout();
        }
        const error = response.text() || response.statusText;
        return Promise.reject(error);
    }
    return response.json();
}

// login
function sign_in(login, password) {
    return fetch(SERVER + LOGIN, {
        method: 'post',
        body: JSON.stringify({login: login, password: password}),
        headers: { 'content-type': 'application/json' }
    })
        .then(fetch_status_check)
        .then(user => {
            sessionStorage.setItem('login', user.login);
            sessionStorage.setItem('password', user.password);
            console.log(user);
            return user;
        })
        ;
}

// registration
function sign_up(login, password, name) {
    return fetch(SERVER + LOGIN, {
        method: 'post',
        body: JSON.stringify({login: login, password: password, name: name}),
        headers: { 'content-type': 'application/json' }
    })
        .then(fetch_status_check)
        .then(user => {
            sessionStorage.setItem('login', user.login);
            sessionStorage.setItem('password', user.password);
            console.log(user);
            return user;
        });
}

function logout() {
    sessionStorage.removeItem('login');
    sessionStorage.removeItem('password');
}

function get_current_user() {
    if (sessionStorage.getItem('login') && sessionStorage.getItem('password'))
        return {"login": sessionStorage.getItem('login'),
            "password": sessionStorage.getItem('password')};
    else {
        if (localStorage.getItem('login') && localStorage.getItem('password')) {
            sessionStorage.setItem('login', localStorage.getItem('login'));
            sessionStorage.setItem('password', localStorage.getItem('password'));
            return {"login": sessionStorage.getItem('login'),
                "password": sessionStorage.getItem('password')};
        }
        return null;
    }
}

function get_data() {
    return fetch(SERVER + GET_DATA, {
        method: 'get'
    })
        .then(fetch_status_check);
}

export {
    logout,
    sign_in,
    sign_up,
    get_current_user,
    get_data
};
