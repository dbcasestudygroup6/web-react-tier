import React from 'react';
import Grid from '../../containers/Grid/Grid';
import { get_data } from '../../api/service'

/*
* Function to get data and format below
*/


/*
getDealData = () => {
  await data = get_data();
  instrumentList = []
  for (var index in data) {
    console.log(index + " : " + data[index].instrumentName);
    instrumentList.push(data[index].instrumentName)
  }
  console.log(instrumentList)
}
*/

const getDealData = () => {
  const data = get_data();
  return data;
}

const getInstrumentList = (data) => {
  var instrumentList = []
  for (var index in data) {
    instrumentList.push(data[index].instrumentName)
  }
  return instrumentList
}


const dashboard = (props) => {
  var instrumentData = getDealData()
  var instrumentNameList = getInstrumentList(instrumentData)
  var graphSizeData = Array(instrumentNameList.length).fill(1)
  return (
    <div >
      <Grid instrumentData={instrumentData} instrumentNameList={instrumentNameList} graphSizeData={graphSizeData}/>
    </div>
  );
}

export default dashboard;