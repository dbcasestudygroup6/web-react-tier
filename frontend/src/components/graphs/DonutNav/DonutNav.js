import React, { Component }  from 'react';
import DonutGraph from './DonutGraph';


class graphnav extends Component {
  constructor(props) {
    super(props);
    this.state={
      instrumentNameList: props.instrumentNameList,
      currentInstrument: props.currentInstrument,
      graphData: {
        labels: props.instrumentNameList,
        datasets: [  
          {
              label: props.currentInstrument.instrumentName,
              backgroundColor: 'rgba(0,0,255,0.4)',
              borderColor: 'rgba(0,0,255,1)',
              data: props.graphSizeData
            }
        ]
      }
    };
  }

  render () {
    return (
      <div>
        <DonutGraph data={this.state.graphData} click={this.props.click}/>
      </div>
    );
  }
}

export default graphnav;