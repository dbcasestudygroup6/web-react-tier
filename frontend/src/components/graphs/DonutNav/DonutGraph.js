import React, {Component} from 'react';
import { Doughnut } from 'react-chartjs-2';




 class GraphNavVisual extends Component {
  

  constructor(props) {
    super(props);
    this.state={
      data: props.data,
      options: {
        onClick: function (evt, item) {
          props.click(item[0]._index);
      }
      }
    };
  }



  render() {
    return (
      <div>
        <h2>Donut Nav</h2>
        <Doughnut ref="chart" 
        data={this.state.data} 
        options={this.state.options}/>
        
      </div>
    );
  }

  componentDidMount() {
    const { datasets } = this.refs.chart.chartInstance.data
    console.log(datasets[0].data);
  }
}

export default GraphNavVisual;

