import React from 'react';
import AverageGraph from './AverageGraph';
/*
* This component will display boxes filled with TBD
*/
const average = (props) => {
  console.log(props.title)
  console.log(props.currentInstrument)
  console.log(props.currentInstrument.average.avgSell)
  console.log(props.currentInstrument.avgBuy)
  const data = {
    labels: [props.title],
    datasets: [
      {
        label: 'Average Sell',
        backgroundColor: 'rgba(255,0,0,0.4)',
        data: [props.currentInstrument.average.avgSell]
      },
      {
          label: 'Average Buy',
          backgroundColor: 'rgba(0,0,255,0.4)',
          data: [props.currentInstrument.average.avgBuy]
        },
        {
          label: 'Current Buy',
          backgroundColor: 'rgba(0,255,255,0.4)',
          data: [props.currentInstrument.average.currentBuy]
        },
        {
          label: 'Current Sell',
          backgroundColor: 'rgba(0,255,0,0.4)',
          data: [props.currentInstrument.average.currentSell]
        }
    ]
  };


  return (
    <div className={props.className}>
      <AverageGraph data={data} />
    </div>
  );
}

export default average;