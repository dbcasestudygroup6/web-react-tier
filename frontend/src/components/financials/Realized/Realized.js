import React from 'react';
import Card from '../Card/Card';
/*
* This component will display boxes filled with TBD
*/
const realized = (props) => {
  return (
    <div className={props.className}>
    <Card pageName={props.pageName}  value={props.value}  />
  </div>
  );
}

export default realized;