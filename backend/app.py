from flask import Flask, request, jsonify
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
import requests

HOST = '0.0.0.0'
PORT = 5000

app = Flask(__name__)
CORS(app)
# username: root
# password: root
# app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://username:password@localhost/db_auth'
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///example.sqlite"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    password = db.Column(db.String, unique=True, nullable=False)
    email = db.Column(db.String, unique=True, nullable=False)
    name = db.Column(db.String, unique=True, nullable=False)


db.create_all()


@app.route('/')
def hello():
    return "<h1>Hello World</h1>"


@app.route('/login', methods=['POST'])
def test():
    if request.method == 'POST':
        # get parameters from post request
        parameters = request.get_json()
        if "name" in parameters.keys():
            print("register", parameters)
            print(db.session.query(User).count())
            db.session.add(User(email=parameters["login"], password=parameters["password"], name=parameters["name"]))
            db.session.commit()
            return jsonify({'login': parameters["login"], 'password': parameters["password"]}), 200
        else:
            print(db.session.query(User).count())
            client = db.session.query(User).filter_by(email=parameters["login"], password=parameters["password"]).first()
            if client is not None:
                print(client)
                print("login", parameters)
                return jsonify(parameters), 200
            else:
                return jsonify({}), 401
    else:
        return jsonify({'not found request': 'Error'}), 401


@app.route('/get_data', methods=['GET'])
def return_data():
    if request.method == 'GET':
        r = requests.get('http://192.168.99.100:8080/get_data')
        if r.status_code == 200:
            return jsonify(r.json()), 200
        print(r.status_code)


if __name__ == '__main__':
    app.run(host=HOST, debug=True, port=PORT)
